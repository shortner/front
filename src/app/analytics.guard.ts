import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from './service/auth.service';
import {UserService} from './service/user.service';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsGuard implements CanActivate {
  constructor(public auth: UserService, public router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const user = this.auth.getUser();
    if (user) {
      return true;
    } else {
      this.router.navigate(['/login'], {
        queryParams: {
          return: state.url
        }
      })
        ;
      return false;
    }
  }
}
