import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {UrlsService} from '../service/urls.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {PreviewComponent} from '../modal/preview/preview.component';
import {UserService} from '../service/user.service';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent implements OnInit {
  status;
  shortInput: FormControl;
  auth;
  constructor(private urls: UrlsService, public navigate: Router, public dialog: MatDialog , public user: UserService) {
    this.shortInput = new FormControl('',  [Validators.minLength(8)]);
    this.auth = this.user.getUser();
  }

  ngOnInit() {
  }

  generateUrl() {
    if (this.shortInput.valid) {
      this.urls.generateURl(this.shortInput.value)
        .subscribe(async (url) => {
        const dil = this.dialog.open(PreviewComponent, {
          data: url,
          disableClose: true
        });
      });
    }
  }

}
