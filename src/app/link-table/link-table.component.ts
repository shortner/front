import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource, MatPaginator, MatDialog} from '@angular/material';
import {UrlsService} from '../service/urls.service';
import {Router} from '@angular/router';
import {PreviewComponent} from '../modal/preview/preview.component';





@Component({
  selector: 'app-link-table',
  templateUrl: './link-table.component.html',
  styleUrls: ['./link-table.component.scss']
})
export class LinkTableComponent implements OnInit {
  columns: string[] = ['origin', 'created', 'short', 'clicks'];
  dataSource ;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(url: UrlsService, public navigator: Router, public dialog: MatDialog) {
    url.getUrls()
      .then((result) => {
        result.subscribe((res: any) => {
          this.dataSource = new MatTableDataSource(res.result);
          this.dataSource.paginator = this.paginator;
        });

      });

  }

  ngOnInit() {

  }

  async analyticView(data) {
    await this.navigator.navigateByUrl(`/analytics/${data.shortCode}`);
  }

  getQR(url) {
    const dialog = this.dialog.open(PreviewComponent, {
      data: {
        shortCode: url
      },
      disableClose: true,
    });
  }

}
