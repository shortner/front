import { Component, OnInit } from '@angular/core';
import {ChartsService} from '../service/charts.service';
import {ActivatedRoute, Router} from '@angular/router';
import {v} from '@angular/core/src/render3';

@Component({
  selector: 'app-analytic',
  templateUrl: './analytic.component.html',
  styleUrls: ['./analytic.component.scss']
})
export class AnalyticComponent implements OnInit {
  id: string;
  data = {};
  about;
  constructor(public chartService: ChartsService, private router: ActivatedRoute) {
    this.router.params.subscribe(url => {
      this.id = url.id;
    });
    this.chartService.getPage(this.id)
      .subscribe((res) => {
        this.about = res['result'][0];
      });
  }

   async ngOnInit() {
     await this.getAnalytic();

   }
  async getAnalytic() {
    const result = await this.chartService.getData(this.id);
    result.subscribe((data) => {
      this.data = {
        os: this.normalize(data['os'], false, 'os'),
        platform: this.normalize(data['platform'], false, 'platform'),
        timeline: this.normalize(data['timeline'], true, 'timeline'),
        referer: this.normalize(data['referer'], false, 'referer'),
        clicks: this.getCount(data['platform'])
      };
    });
  }
  private normalize(data, series: boolean, name?: string) {
    if (series) {
      const result = {
        name: this.id,
        series: []
      };
      result.series = data.map((value => {
        return {name: new Date(value.time), value: value.count_url_id};
      }));
      return result;
    } else {
      let result_ = [];
      result_ = data.map(value => {
        return {name: value[name], value: value.count_url_id};
      });
      return result_;
    }
  }
  private getCount(data) {
    if (data.length > 1) {
      return data.reduce((prev, cur) => {
        if (typeof prev === 'object') {
          return prev.count_url_id + cur.count_url_id;
        }
        return prev + cur.count_url_id;
      });
    } else {
      return data[0].count_url_id;
    }
  }


}


// function formateSeries(data) {
//   data = data.map((value) => {
//     return {
//       name: value[]
//     }
//   })
// }





