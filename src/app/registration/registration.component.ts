import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {AuthService, UserResponse} from '../service/auth.service';
import {Router} from '@angular/router';
import {UserService} from '../service/user.service';
import {ancestorWhere} from 'tslint';



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  login: FormControl;
  password: FormControl;
  press: boolean;
  status;
  constructor(public auth: AuthService, public navigate: Router, public userService: UserService) {
    this.login = new FormControl('', [Validators.required, Validators.email]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(8)]);
  }

  ngOnInit() {
  }

  singUp() {
    this.press = true;
    if (this.login.valid && this.password.valid) {
      const registration = this.auth.registration(this.login.value, this.password.value)
      registration.subscribe(async (user: UserResponse) => {
          if (user.status) {
            localStorage.setItem('token', user.token);
            this.userService.setUser(user);
            await this.navigate.navigate(['/']);
          } else {
            this.status = true;
          }
        },
        error1 => console.error(error1)
      );
    }
  }

}
