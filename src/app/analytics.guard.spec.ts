import { TestBed, async, inject } from '@angular/core/testing';

import { AnalyticsGuard } from './analytics.guard';

describe('AnalyticsGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnalyticsGuard]
    });
  });

  it('should ...', inject([AnalyticsGuard], (guard: AnalyticsGuard) => {
    expect(guard).toBeTruthy();
  }));
});
