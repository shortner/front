import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {
  host = '/q/' + this.data.shortCode;

  constructor(private dialogRef: MatDialogRef<PreviewComponent>, @Inject(MAT_DIALOG_DATA) public data, public router: Router) { }

  ngOnInit() {
  }
  public async closeDialog() {
    this.dialogRef.close();
    window.location.reload();
  }

}
