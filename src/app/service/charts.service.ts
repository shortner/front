import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChartsService {

  host: string = environment.host;
  api: string = environment.api;
  version: string = environment.version;

  constructor(private http: HttpClient) { }

  public async getData(id: string) {
    return await this.http.get(`${this.host}/${this.api}/account/analytic/${id}`);
  }

  public getPage(id) {
    return this.http.get(`${this.host}/${this.api}/account/analytic-page/${id}`);
  }

}


export interface ChartResponse {
  os: OSResponse[];
  platform: PlatformResponse[];
  timeline: TimeResponse[];
}

export interface OSResponse {
  time: Date;
  count_url_id: number;
  os: string;
}
export interface PlatformResponse {
  time: Date;
  count_url_id: number;
  os: string;
}
export interface TimeResponse {
  time: Date;
  count_url_id: number;
}
