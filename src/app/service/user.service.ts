import { Injectable } from '@angular/core';
import {UserResponse} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user;
  constructor() { }

  getUser() {
    const user = localStorage.getItem('user');
    return JSON.parse(user);
  }
  setUser(user) {
    localStorage.setItem('user', JSON.stringify(user));
    this.user = user;
  }
  removeUser() {
    localStorage.clear();
  }
}


