import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  host: string = environment.host;
  api: string = environment.api;
  version: string = environment.version;

  constructor(public http: HttpClient) { }
  login(login, password) {
    return this.http.post(`${this.host}/api/auth/sign-in/`, {email: login, password: password});
  }

  registration(login, password) {
    return  this.http.post(`${this.host}/api/auth/sign-up/`, {email: login, password: password});
  }
  status(token) {
    return this.http.post(`${this.host}/api/auth/status/`, {token: token});
  }

}


export interface UserResponse {
  status: boolean;
  message: string;
  token?: string;
  username?: string;
}
