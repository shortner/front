import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UrlsService {
  host = environment.host;
  api = environment.api;

  constructor(public http: HttpClient) { }

  public async getUrls() {
    return await this.http.get(`${this.host}/${this.api}/account`);
  }

  public  generateURl(url) {
    return  this.http.post(`${this.host}/${this.api}/account`, {'origin': url});
  }
  public checkUrl(url) {
    return this.http.get(url);
  }
}
