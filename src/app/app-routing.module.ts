import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AnalyticComponent} from './analytic/analytic.component';
import {MainComponent} from './main/main.component';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {AnalyticsGuard} from './analytics.guard';

const routes: Routes = [
  {path: 'analytics/:id', canActivate: [AnalyticsGuard] , component: AnalyticComponent},
  {path: '', component: MainComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
