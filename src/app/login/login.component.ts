import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {AuthService, UserResponse} from '../service/auth.service';
import {Router} from '@angular/router';
import {UserService} from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  login: FormControl;
  password: FormControl;
  press: boolean;
  status: boolean;
  constructor(public auth: AuthService, public navigate: Router, public userService: UserService) {
    this.login = new FormControl('', [Validators.required, Validators.email]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(8)]);
  }

  ngOnInit() {
  }
  singIn() {
    this.press = true;
    if (this.login.valid && this.password.valid) {
      const login = this.auth.login(this.login.value, this.password.value)
      login.subscribe(async (user: UserResponse) => {
        if (user.status) {
          localStorage.setItem('token', user.token);
          this.userService.setUser(user);
          await this.navigate.navigate(['/']);
        } else {
          this.status = true;
        }
      },
        error1 => {
        this.login.reset();
        this.password.reset();
        });
    }
  }
}
