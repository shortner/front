import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule, MatProgressBarModule,
  MatTableModule
} from '@angular/material';
import { AppComponent } from './app.component';
import { InputFieldComponent } from './input-field/input-field.component';
import { HeaderComponent } from './header/header.component';
import { AboutComponent } from './about/about.component';
import { LinkTableComponent } from './link-table/link-table.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import { AnalyticComponent } from './analytic/analytic.component';
import {AppRoutingModule} from './app-routing.module';
import { MainComponent } from './main/main.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {ChartsService} from './service/charts.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import {AuthInterceptor} from './interceptor/auth.interceptor';
import {UserService} from './service/user.service';
import { PreviewComponent } from './modal/preview/preview.component';
import {QRCodeModule} from 'angularx-qrcode';
import { ChartComponent } from './chart/chart.component';
import { RegistrationComponent } from './registration/registration.component';

@NgModule({
  declarations: [
    AppComponent,
    InputFieldComponent,
    HeaderComponent,
    AboutComponent,
    LinkTableComponent,
    AnalyticComponent,
    MainComponent,
    LoginComponent,
    SignupComponent,
    PreviewComponent,
    ChartComponent,
    RegistrationComponent,
  ],
  imports: [
    BrowserModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    NoopAnimationsModule,
    MatPaginatorModule,
    MatMenuModule,
    AppRoutingModule,
    NgxChartsModule,
    HttpClientModule,
    MatDividerModule,
    MatCardModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    MatDialogModule,
    QRCodeModule
  ],
  providers: [
    ChartsService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    UserService
  ],
  entryComponents: [PreviewComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
