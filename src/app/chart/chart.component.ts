import {Component, OnChanges, OnInit, Input, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnChanges {
  @Input() data;
  colors = {
    domain: ['#3f51b5']
  };
  clicks: 0;

  constructor() {

  }

  ngOnChanges() {
    Object.assign(this, this.data);
    this.clicks = this.getClicks();
    this.getTodayClicks();
    console.log(this.data)
  }

  private getClicks() {
    if (this.data.clicks) {
      return this.data.clicks;
    }
    return 0;
  }

  private getTodayClicks() {
    const today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    if (this.data) {
      if (this.data.timeline.series.length > 1) {
        this.data.todayClicks = this.data.timeline.series.reduce((prev, curr) => {
          const recordTime = new Date(curr.name).getTime();
          if (today.getTime() < recordTime ) {
            if (typeof prev === 'object') {
              return prev.value + curr.value;
            }
            return prev + curr.value;
          }
        });
        this.data.precentTodayClicks = Math.round((this.data.todayClicks * 100) / this.clicks);
      } else {
        this.data.todayClicks = this.clicks;
        this.data.precentTodayClicks = Math.round((this.data.timeline.series[0].value *  100) / this.clicks);
      }
    }
  }

}
