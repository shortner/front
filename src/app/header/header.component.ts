import { Component, OnInit } from '@angular/core';
import {UserService} from '../service/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  auth;
  constructor(public user: UserService) {
    this.auth = this.user.getUser();
  }

  ngOnInit() {
  }

  logout() {
    this.user.removeUser();
    window.location.reload();
  }

}
