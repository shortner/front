import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(req: HttpRequest<any>, next: HttpHandler) {

    const token = localStorage.getItem('token');
    if (token) {

      const request = req.clone({setHeaders: {Authorization: 'Bearer ' + token}});
      return next.handle(request);
    } else {
      return next.handle(req);
    }
  }
}
