import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {UserService} from '../service/user.service';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {


  constructor(public user: UserService, public auth: AuthService) {
    const token = localStorage.getItem('token');
    this.auth.status(token)
        .subscribe((value) => {
          this.user.setUser(value);
        });
  }

  ngOnInit() {
  }

}
